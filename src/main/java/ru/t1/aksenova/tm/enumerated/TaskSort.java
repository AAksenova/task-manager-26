package ru.t1.aksenova.tm.enumerated;

import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.comparator.CreatedComparator;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.comparator.StatusComparator;
import ru.t1.aksenova.tm.model.Task;

import java.util.Comparator;

public enum TaskSort {

    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare);

    @Nullable
    private final String name;

    @Nullable
    private final Comparator<Task> comparator;

    @Nullable
    public static TaskSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final TaskSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

    TaskSort(
            @Nullable final String name,
            @Nullable final Comparator<Task> comparator
    ) {
        this.name = name;
        this.comparator = comparator;
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nullable
    public Comparator<Task> getComparator() {
        return comparator;
    }

}
